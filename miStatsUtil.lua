miDebugMode = false

function printDebugInfo(...)
	if miDebugMode then
		print(...)
	end
end



local waitTable = {};
local waitFrame = nil;

function miWait(delay, func, ...)
  if(type(delay)~="number" or type(func)~="function") then
	  printDebugInfo("failed wait")
    return false;
  end
  if(waitFrame == nil) then
    waitFrame = CreateFrame("Frame","WaitFrame", UIParent);
    waitFrame:SetScript("onUpdate",function (self,elapse)
      local count = #waitTable;
      local i = 1;
      while(i<=count) do
        local waitRecord = tremove(waitTable,i);
        local d = tremove(waitRecord,1);
        local f = tremove(waitRecord,1);
        local p = tremove(waitRecord,1);
        if(d>elapse) then
          tinsert(waitTable,i,{d-elapse,f,p});
          i = i + 1;
        else
          count = count - 1;
          f(unpack(p));
        end
      end
    end);
  end
  tinsert(waitTable,{delay,func,{...}});
  return true;
end


function miColorLookup(color)
	local rColor = {}
	local supported = false
	if color == "red" then
		rColor.r = 0.95
		rColor.g = 0.2
		rColor.b = 0.11
		supported = true
	elseif color == "green" then
		rColor.r = 0.12
		rColor.g = 0.97
		rColor.b = 0.11
		supported = true
	elseif color == "white" then
		rColor.r = 1
		rColor.g = 1
		rColor.b = 1
		supported = true
	elseif color == "black" then
		rColor.r = 0
		rColor.g = 0
		rColor.b = 0
		supported = true
	elseif color == "grey" or color == "gray" then
		rColor.r = 0.67
		rColor.g = 0.67
		rColor.b = 0.67
		supported = true
	elseif color == "purple" then
		rColor.r = 0.9
		rColor.g = 0.05
		rColor.b = 1
		supported = true
	elseif color == "orange" then
		rColor.r = 0.89
		rColor.g = 0.52
		rColor.b = 0.22
		supported = true
	elseif color == "yellow" then
		rColor.r = 0.89
		rColor.g = 0.85
		rColor.b = 0.31
		supported = true
	elseif color == "blue" then
		rColor.r = 0.2
		rColor.g = 0.15
		rColor.b = 0.89
		supported = true
	elseif color == "mage" or color == "warlock" or color == "rogue" or color == "druid" or color == "warrior" or color == "priest" or color == "hunter" or color == "paladin" or color == "shaman" or color == "monk" or color == "deathknight" or color == "demonhunter" then
		local upperCase = strupper(color)
		rColor.r = RAID_CLASS_COLORS[upperCase].r
		rColor.g = RAID_CLASS_COLORS[upperCase].g
		rColor.b = RAID_CLASS_COLORS[upperCase].b
		supported = true
	elseif color == "class" then
		local _, playerClass, _ = UnitClass("player")
		rColor.r = RAID_CLASS_COLORS[playerClass].r
		rColor.g = RAID_CLASS_COLORS[playerClass].g
		rColor.b = RAID_CLASS_COLORS[playerClass].b
		supported = true
	end
	return rColor, supported
end
