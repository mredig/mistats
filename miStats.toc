## Interface: 90001
## Title: miStats
## Notes: Quick access to view your fps, network lag, and estimated kills/quests until you level
## Author: Deathbypizza
## Version: v0.6
## SavedVariables: miStatsToggles, miStatsFontSize, miStatsOneLine
## SavedVariablesPerCharacter: miStatsXPCache, miStatsShadow, miStatsColor

miStatsUtil.lua
miStats.lua
main.lua
