local inited = false
local addonFrame -- access to frame object
local theText -- access to the text object
local updateCooldown = 1
local updateAccumulation = 0

local events = {}
local xp, levelMaxXP, playerLevel
local ktl, qtl, dtl = 0, 0, 0
local avgKill, avgQuest, avgDung = 0, 0, 0
local isInDungeon = 0
local dungeonMapID = ""
local dungeonFinished = nil

local guidCache = {}
local dungeonCache = {}

local kMethodMedian, kMethodMean = 0, 1
local kCacheKillsStr, kCacheQuestsStr, kCacheDungeonsStr = "kills", "quests", "dungeons"
local kTogglesFPSStr, kToggleKTLStr, kToggleLagStr, kToggleQTLStr, kToggleDTLStr  = "fps", "xpkill", "lag", "xpquest", "dungeonxp"
local kTogglesShowAvg = "showAverages"
local kMaxKillArrayLength = 10
local kMaxQuestArrayLength = 6

local newXPComesFromKill = false


-- azerite stuff
local azInstanceCache, azWQCache = {}, {}
local kCacheAzDungeonsStr, kCacheAzWQuestsStr, kCacheAzIslandsStr = "azDungeons", "azWQuests", "azIslands"
local kToggleAzDTLStr, kToggleAzWQTLStr, kToggleAzITLStr  = "azeriteDungeon", "azeriteWorldQuest", "azeriteIsland"
local HasActiveAzeriteItem, FindActiveAzeriteItem, GetAzeriteItemXPInfo, GetPowerLevel = C_AzeriteItem.HasActiveAzeriteItem, C_AzeriteItem.FindActiveAzeriteItem, C_AzeriteItem.GetAzeriteItemXPInfo, C_AzeriteItem.GetPowerLevel
local currentAzeriteXP, maxAzeriteXP = 0, 0
local isInExpedition = 0
local azIslandsTL, azDungeonsTL, azWorldQuestsTL = 0, 0, 0
local azAvgIsland, azAvgDung, azAvgWQ = 0, 0, 0
local kMaxAzDungeonArrayLength = 10
local kMaxAzIslandArrayLength = 10
local kMaxAzWQArrayLength = 10
local activeWQ = nil


local function updateFrameSize()
	local width = theText:GetStringWidth()/addonFrame:GetEffectiveScale()
	local height = theText:GetStringHeight()/addonFrame:GetEffectiveScale()
	addonFrame:SetWidth(width)
	addonFrame:SetHeight(height)
end

local function updateStats()
	if not inited then --only proceed if initialization has occurred
		return
	end
	local outputStr = ""
	local oneLineStr = "\n"
	if miStatsOneLine then
		oneLineStr = "  "
	end
	if miStatsToggles[kTogglesFPSStr] then
		local fps = floor(GetFramerate())
		outputStr = outputStr .. "FPS: " .. fps .. oneLineStr
	end

	if miStatsToggles[kToggleLagStr] then
		local down, up, lagHome, lagWorld = GetNetStats();
		outputStr = outputStr .. "Home: " .. lagHome .. "ms" .. oneLineStr
		outputStr = outputStr .. "World: " .. lagWorld .. "ms" .. oneLineStr
	end

	if playerLevel < MAX_PLAYER_LEVEL then
		if miStatsToggles[kToggleKTLStr] then
			outputStr = outputStr .. "KTL: " .. ktl
			if miStatsToggles[kTogglesShowAvg] then
				outputStr = outputStr .. " (" .. avgKill .. "/kill)"
			end
			outputStr = outputStr .. oneLineStr
		end

		if miStatsToggles[kToggleQTLStr] then
			outputStr = outputStr .. "QTL: " .. qtl
			if miStatsToggles[kTogglesShowAvg] then
				outputStr = outputStr .. " (" .. avgQuest .. "/quest)"
			end
			outputStr = outputStr .. oneLineStr
		end

		if miStatsToggles[kToggleDTLStr] then
			outputStr = outputStr .. "DTL: " .. dtl
			if miStatsToggles[kTogglesShowAvg] then
				outputStr = outputStr .. " (" .. avgDung .. "/dungeon)"
			end
			outputStr = outputStr .. oneLineStr
		end
	else
		if miStatsToggles[kToggleAzDTLStr] then
			outputStr = outputStr .. "azDTL: " .. azDungeonsTL
			if miStatsToggles[kTogglesShowAvg] then
				outputStr = outputStr .. " (" .. azAvgDung .. "/dungeon)"
			end
			outputStr = outputStr .. oneLineStr
		end

		if miStatsToggles[kToggleAzWQTLStr] then
			outputStr = outputStr .. "azWQTL: " .. azWorldQuestsTL
			if miStatsToggles[kTogglesShowAvg] then
				outputStr = outputStr .. " (" .. azAvgWQ .. "/wq)"
			end
			outputStr = outputStr .. oneLineStr
		end

		if miStatsToggles[kToggleAzITLStr] then
			outputStr = outputStr .. "azITL: " .. azIslandsTL
			if miStatsToggles[kTogglesShowAvg] then
				outputStr = outputStr .. " (" .. azAvgIsland .. "/island)"
			end
			outputStr = outputStr .. oneLineStr
		end
	end

	if miDebugMode then
		outputStr = outputStr .. "is in dungeon: " .. isInDungeon .. oneLineStr
		outputStr = outputStr .. "map ID: " .. dungeonMapID .. oneLineStr
	end

	theText:SetFormattedText(outputStr)
	updateFrameSize()
end

local function updateGameLoop(self, elapsed)
	updateAccumulation = updateAccumulation + elapsed

	if updateAccumulation >= updateCooldown then
		updateAccumulation = 0
		-- print("updating", updateAccumulation)
		updateStats()
	end
end



local function UnitInfoFromGuid(guid)
    local parts = {strsplit('-', guid)}
    local type = parts[1]

    if type == 'Creature' or type == 'Vehicle' then
        local id = tonumber(parts[6])
        return type, id
    end

    return type
end


local function addKillToCache(killXP)
	table.insert(miStatsXPCache[kCacheKillsStr], killXP)
	table.sort(miStatsXPCache[kCacheKillsStr])
	while #miStatsXPCache[kCacheKillsStr] > kMaxKillArrayLength do
		table.remove(miStatsXPCache[kCacheKillsStr], 1)
		table.remove(miStatsXPCache[kCacheKillsStr])
	end
end


local function addQuestToCache(questXP)
	-- print("adding to quest cache: ", questXP)
	table.insert(miStatsXPCache[kCacheQuestsStr], questXP)
	table.sort(miStatsXPCache[kCacheQuestsStr])

	while #miStatsXPCache[kCacheQuestsStr] > kMaxQuestArrayLength do
		table.remove(miStatsXPCache[kCacheQuestsStr], 1)
		table.remove(miStatsXPCache[kCacheQuestsStr])
	end
end

local function addDungeonToCache(dungeonXP)
	if dungeonXP == 0 then
		return
	end
	table.insert(miStatsXPCache[kCacheDungeonsStr], dungeonXP)
	table.sort(miStatsXPCache[kCacheDungeonsStr])
	printDebugInfo("added dungeon to cache: ", dungeonXP)
end

local function addAzDungeonToCache(dungeonAP)
	if dungeonAP == 0 then
		return
	end
	table.insert(miStatsXPCache[kCacheAzDungeonsStr], dungeonAP)
	table.sort(miStatsXPCache[kCacheAzDungeonsStr])

	while #miStatsXPCache[kCacheAzDungeonsStr] > kMaxAzDungeonArrayLength do
		table.remove(miStatsXPCache[kCacheAzDungeonsStr], 1)
		table.remove(miStatsXPCache[kCacheAzDungeonsStr])
	end
end

local function addAzWQToCache(worldQuestAP)
	if worldQuestAP == 0 then
		return
	end
	table.insert(miStatsXPCache[kCacheAzWQuestsStr], worldQuestAP)
	table.sort(miStatsXPCache[kCacheAzWQuestsStr])
	printDebugInfo("added wq to cache: ", worldQuestAP)
end

local function addAzIslandToCache(islandAP)
	if islandAP == 0 then
		return
	end
	table.insert(miStatsXPCache[kCacheAzIslandsStr], islandAP)
	table.sort(miStatsXPCache[kCacheAzIslandsStr])
	printDebugInfo("added island to cache: ", islandAP)
end

local function completeXPDungeon(dungeonID)
	local total = 0
	if dungeonCache[dungeonID] == nil then
		return 0
	end
	for i,v in ipairs(dungeonCache[dungeonID]) do
		total = total + v
	end
	dungeonCache[dungeonID] = {}
	return total
end

local function completeAPInstance(dungeonID) -- used for both dungeons and islands
	local total = 0
	if azInstanceCache[dungeonID] == nil then
		return 0
	end
	for i,v in ipairs(azInstanceCache[dungeonID]) do
		total = total + v
	end
	azInstanceCache[dungeonID] = {}
	return total
end

local function calculateAverage(array, method)
	if array == nil or #array <= 0 then -- if the array is empty, just return 1 to prevent dividing by 0
		return 1
	end
	if method == kMethodMean then
		local total = 0
		for i,v in ipairs(array) do
			total = total + v
		end
		return total / #array
	elseif method == kMethodMedian then
		local halfwayA
		local halfwayB
		if #array % 2 == 0 then
			halfwayA = #array/2
			halfwayB = (#array/2) + 1
		else
			halfwayA = ceil(#array/2)
			halfwayB = halfwayA
		end
		local avg = (array[halfwayA] + array[halfwayB]) / 2
		-- printDebugInfo("arrayLength, halfwayA, halfwayB, avg", #array, halfwayA, halfwayB, avg)
		return avg
	end
end

local function calculateDTL()
	local remaining = levelMaxXP - xp
	avgDung = calculateAverage(miStatsXPCache[kCacheDungeonsStr], kMethodMedian)
	-- print("remaining, avgDung", remaining, avgDung)
	dtl = ceil(remaining / avgDung)
	if dtl >= remaining then
		-- print("remaining:" .. remaining .. " max:" .. levelMaxXP .. " xp:" .. xp .. " avgDung:" .. avgDung .. " ktl:" .. ktl)
		dtl = "calculating..."
	end
end

local function calculateQTL()
	local remaining = levelMaxXP - xp
	avgQuest = calculateAverage(miStatsXPCache[kCacheQuestsStr], kMethodMedian)
	-- print("remaining, avgQuest", remaining, avgQuest)
	qtl = ceil(remaining / avgQuest)
	if qtl >= remaining then
		-- print("remaining:" .. remaining .. " max:" .. levelMaxXP .. " xp:" .. xp .. " avgQuest:" .. avgQuest .. " ktl:" .. ktl)
		qtl = "calculating..."
	end
end

local function calculateKTL()
	local remaining = levelMaxXP - xp
	avgKill = calculateAverage(miStatsXPCache[kCacheKillsStr], kMethodMedian)
	ktl = ceil(remaining / avgKill)
	if ktl >= remaining then
		-- print("remaining:" .. remaining .. " max:" .. levelMaxXP .. " xp:" .. xp .. " avgKill:" .. avgKill .. " qtl:" .. qtl)
		ktl = "calculating..."
	end
end

local function calculateAzDTL()
	local remaining = maxAzeriteXP - currentAzeriteXP
	azAvgDung = calculateAverage(miStatsXPCache[kCacheAzDungeonsStr], kMethodMedian)
	azDungeonsTL = ceil(remaining / azAvgDung)
	if azDungeonsTL >= remaining then
		azDungeonsTL = "calculating..."
	end
end

local function calculateAzWQTL()
	local remaining = maxAzeriteXP - currentAzeriteXP
	azAvgWQ = calculateAverage(miStatsXPCache[kCacheAzWQuestsStr], kMethodMedian)
	azWorldQuestsTL = ceil(remaining / azAvgWQ)
	if azWorldQuestsTL >= remaining then
		azWorldQuestsTL = "calculating..."
	end
end

local function calculateAzITL()
	local remaining = maxAzeriteXP - currentAzeriteXP
	azAvgIsland = calculateAverage(miStatsXPCache[kCacheAzIslandsStr], kMethodMedian)
	azIslandsTL = ceil(remaining / azAvgIsland)
	if azIslandsTL >= remaining then
		azIslandsTL = "calculating..."
	end
end

local function calculateToLevels()
	if not inited then
		-- print("need to init still")
		return
	end
	calculateKTL()
	calculateQTL()
	calculateDTL()
	calculateAzDTL()
	calculateAzWQTL()
	calculateAzITL()
	updateStats()
end


function resetXPCache()
	miStatsXPCache = {}
	miStatsXPCache[kCacheQuestsStr] = {}
	miStatsXPCache[kCacheKillsStr] = {}
	miStatsXPCache[kCacheDungeonsStr] = {}
	miStatsXPCache[kCacheAzDungeonsStr] = {}
	miStatsXPCache[kCacheAzWQuestsStr] = {}
	miStatsXPCache[kCacheAzIslandsStr] = {}
end

local function playerLevelDelay()
	playerLevel = UnitLevel("player")
	printDebugInfo("Player is now level ", playerLevel)
	calculateToLevels()
end

function events:PLAYER_LEVEL_UP(...)
	-- print("level up!")
	playerLevel = UnitLevel("player")
	printDebugInfo("Player is now level ", playerLevel)
	miWait(1, playerLevelDelay)
	if playerLevel % 10 == 0 then
		resetXPCache()
	end
	calculateToLevels()
end

function events:PLAYER_XP_UPDATE(...)
	local newXpAmount = UnitXP("player")
	local amountGained = newXpAmount - xp
	xp = newXpAmount
	levelMaxXP = UnitXPMax('player')
	-- print("xp gained!", amountGained)
	if amountGained <= 0 or playerLevel == MAX_PLAYER_LEVEL then -- can be a negative value when leveling, so just discard this result and ignore it
		newXPComesFromKill = false
		return
	end
	if newXPComesFromKill then
		newXPComesFromKill = false
		addKillToCache(amountGained)
	end

	if isInDungeon == 1 then
		if dungeonCache[dungeonMapID] == nil then
			dungeonCache[dungeonMapID] = {}
		end
		table.insert(dungeonCache[dungeonMapID], amountGained)
		printDebugInfo("instance xp gain: ", amountGained)

	end

	calculateToLevels()

end

local function dungeonXPFinishUp()
	local dungeonXP = completeXPDungeon(dungeonMapID)
	printDebugInfo("finished dungeon with " .. dungeonXP .. " xp")
	addDungeonToCache(dungeonXP)
	calculateToLevels()
end

local function dungeonAPFinishUp()
	local dungeonAP = completeAPInstance(dungeonMapID)
	printDebugInfo("finished dungeon with " .. dungeonAP .. " ap")
	addAzDungeonToCache(dungeonAP)
	calculateToLevels()
end

local function islandAPFinishUp()
	local islandAP = completeAPInstance(dungeonMapID)
	printDebugInfo("finished island with " .. islandAP .. " ap")
	addAzIslandToCache(islandAP)
	calculateToLevels()
end

local function wqAPFinishUp()
	local wqAP = completeAPInstance(activeWQ)
	printDebugInfo("finished world quest with " .. wqAP .. " ap")
	addAzWQToCache(wqAP)
	calculateToLevels()
end

function events:SCENARIO_COMPLETED(...)
	printDebugInfo("SCENARIO_COMPLETED", ...)
	if isInDungeon == 1 then
		miWait(1, dungeonXPFinishUp)
		dungeonFinished = dungeonMapID
	end


end

function events:ISLAND_COMPLETED(...)
	if isInExpedition == 1 then
		miWait(1, islandAPFinishUp)
	end
end


function events:PLAYER_REGEN_ENABLED(...) -- leave combat
	updateCooldown = 1
end

function events:PLAYER_REGEN_DISABLED(...) -- enter combat
	updateCooldown = 2
end


function events:COMBAT_LOG_EVENT_UNFILTERED( ...)
	local timestamp, eventType, hideCaster, sourceGUID, sourceName, sourceFlags, sourceRaidFlags, destGUID, destName, destFlags, destRaidFlags, spellId, spellName, spellSchool, amount, overkill, school, resisted, blocked, absorbed, critical, glancing, crushing = CombatLogGetCurrentEventInfo()
	if destGUID and playerLevel < MAX_PLAYER_LEVEL then
		-- print("dest guid exists")

		local type, id = UnitInfoFromGuid(destGUID)
		-- print("type, id: ", type, id)
		if type == 'Creature' or type == 'Vehicle' then
			-- print("type valid, event type: ", eventType)
			if eventType:match('_DAMAGE$') then
				-- print("matches damage")
                if sourceGUID == UnitGUID('player') then
					-- print("matches player")
                    if guidCache[destGUID] == nil then
						-- print("logging target guid")
                        guidCache[destGUID] = 1
                    end
                end

            elseif eventType == 'UNIT_DIED' or eventType == 'PARTY_KILL' then
                if guidCache[destGUID] and guidCache[destGUID] ~= 0 then
					guidCache[destGUID] = 0
					newXPComesFromKill = true
                end
            end
		end
	end

end

local function hookWorldQuests() -- set up hook to detect entering a wq area
	hooksecurefunc("ObjectiveTracker_Update", function(reason, questID)
		if (UnitIsDeadOrGhost("player") == false and activeWQ == nil and reason == OBJECTIVE_TRACKER_UPDATE_WORLD_QUEST_ADDED) then
			activeWQ = questID
			printDebugInfo("Entering WQ zone for ", questID)
		end
	end)
end

local function clearActiveWQ()
	activeWQ = nil
end
function events:QUEST_REMOVED(...) -- player has left the quest zone or finished the wq
	local questID = ...
	printDebugInfo("quest removed: ", questID)
	if questID == activeWQ then
		miWait(2, clearActiveWQ) -- wait 2 sec to confirm that wq turn in completes first
	end
end

local function azeriteInit()
	if not HasActiveAzeriteItem() then
		return
	end

	hookWorldQuests()

	local azItemLoc = FindActiveAzeriteItem()
	currentAzeriteXP, maxAzeriteXP = GetAzeriteItemXPInfo(azItemLoc)

	printDebugInfo("az info: ", currentAzeriteXP, maxAzeriteXP)

end

function events:AZERITE_ITEM_EXPERIENCE_CHANGED(...) -- gain azerite power from any source
	local azItemLoc, oldXPAmount, newXPAmount = ...
	local amountGained = newXPAmount - oldXPAmount

	printDebugInfo("AZERITE_ITEM_EXPERIENCE_CHANGED", azItemLoc, oldXPAmount, newXPAmount, amountGained)

	if isInDungeon == 1 then
		if azInstanceCache[dungeonMapID] == nil then
			azInstanceCache[dungeonMapID] = {}
		end
		table.insert(azInstanceCache[dungeonMapID], amountGained)
		printDebugInfo("instance ap gain: ", amountGained)
	end

	if isInExpedition == 1 then
		if azInstanceCache[dungeonMapID] == nil then
			azInstanceCache[dungeonMapID] = {}
		end
		table.insert(azInstanceCache[dungeonMapID], amountGained)
		printDebugInfo("island ap gain: ", amountGained)
	end

	if activeWQ ~= nil and isInDungeon ~= 1 and isInExpedition ~= 1 then
		if azInstanceCache[activeWQ] == nil then
			printDebugInfo("adding to wq with ID: ", activeWQ)
			azInstanceCache[activeWQ] = {}
		end
		table.insert(azInstanceCache[activeWQ], amountGained)
		printDebugInfo("world quest ap gain: ", amountGained)
	end

	miWait(1, calculateToLevels)
	-- calculateToLevels()
end

function events:AZERITE_ITEM_POWER_LEVEL_CHANGED(...) -- heart of azeroth level up (item experience changed SHOULD run prior)
	local azItemLoc, oldPowerLevel, newPowerLevel, unlockedEmpoweredItemsInfo = ...
	local levelGain = newPowerLevel - oldPowerLevel

	printDebugInfo("AZERITE_ITEM_POWER_LEVEL_CHANGED", azItemLoc, oldPowerLevel, newPowerLevel, levelGain)
	currentAzeriteXP, maxAzeriteXP = GetAzeriteItemXPInfo(azItemLoc)

	calculateToLevels()

end


function events:QUEST_TURNED_IN(questID, experience, money)
	if experience > 0 then
		addQuestToCache(experience)
	end

	if questID == activeWQ then
		miWait(1, wqAPFinishUp) -- wait 1 sec to let the bonus item ap contribute before tallying results, but keep at 1 sec so quest removed doesnt happen first
	end

	calculateToLevels()
end


local function initialValues() -- gets called with every reload - be sure to accommodate
	--configure global vars
	xp = UnitXP("player")
	levelMaxXP = UnitXPMax('player')
	playerLevel = UnitLevel("player")

	--configure toggle defaults
	local toggles = {kTogglesFPSStr, kToggleKTLStr, kToggleLagStr, kToggleQTLStr, kToggleDTLStr, kToggleAzDTLStr, kToggleAzWQTLStr, kToggleAzITLStr, kTogglesShowAvg}
	if miStatsToggles == nil then
		miStatsToggles = {}
	end
	for i,value in ipairs(toggles) do
		if miStatsToggles[value] == nil then
			miStatsToggles[value] = true
		end
	end

	--configure font size
	if miStatsFontSize == nil then
		miStatsFontSize = 14
	end

	--configure saved disk cache
	local cacheStrs = {kCacheKillsStr, kCacheQuestsStr, kCacheDungeonsStr, kCacheAzDungeonsStr, kCacheAzWQuestsStr, kCacheAzIslandsStr}
	if miStatsXPCache == nil then
		resetXPCache()
	end
	for i,value in ipairs(cacheStrs) do
		if miStatsXPCache[value] == nil then
			miStatsXPCache[value] = {}
		end
	end

	azeriteInit()

	if miStatsOneLine == nil then
		miStatsOneLine = false
	end

	if miStatsShadow == nil then
		miStatsShadow = true
	end

	-- configure default color settings
	if miStatsColor == nil then
		miStatsColor = miColorLookup("class")
	end

	-- cleanCaches()

	calculateToLevels()
end

-- one of these might be able to be removed?

function events:ADDON_LOADED(...)
	initialValues()
	calculateToLevels()
end

function handleDungeonEnter() -- gets called after a delay of changing zones - will determine the zone type (dungeon/island expedition/world/etc)
	local instanceName, instanceType, difficultyID, difficultyName, maxPlayers, dynamicDifficulty, isDynamic, instanceMapID, instanceGroupSize = GetInstanceInfo()

	printDebugInfo(instanceName, instanceType, instanceMapID, instanceGroupSize)

	if instanceType == "party" then
		isInDungeon = 1
		dungeonMapID = instanceName .. "-" .. instanceMapID
	elseif instanceType == "scenario" then
		isInExpedition = 1
		dungeonMapID = instanceName .. "-" .. instanceMapID
	else
		isInDungeon = 0
		dungeonMapID = ""

	end
end

function events:PLAYER_ENTERING_WORLD(...)
	initialValues()
	calculateToLevels()
	miWait(5, handleDungeonEnter)
	-- _wait(2.5,handleDungeonEnter);
	-- handleDungeonEnter(GetInstanceInfo())
end

function events:ZONE_CHANGED_NEW_AREA(...)
	printDebugInfo("new zone! guid cache cleared",  ...)
	miWait(10, handleDungeonEnter)

	if dungeonFinished ~= nil then
		dungeonAPFinishUp()
	end

	-- handleDungeonEnter(GetInstanceInfo())
	guidCache = {}
end



local function setmiStatsTextColor(newColor)
	if miStatsColor == nil and newColor == nil then
		return
	elseif newColor ~= nil then
		local theColor, success = miColorLookup(newColor)
		printDebugInfo(theColor, success)
		if success then
			miStatsColor = theColor
		else
			print("Sorry, that's not a valid color.")
		end
	end
	theText:SetTextColor(miStatsColor.r, miStatsColor.g, miStatsColor.b)
end

local function createFrame()
	addonFrame = CreateFrame("Button", "miStats", UIParent)
	addonFrame:SetHeight(20)
	addonFrame:SetPoint("CENTER")

	addonFrame:SetMovable(true)
	addonFrame:EnableMouse(true)
	addonFrame:RegisterForDrag("LeftButton")
	addonFrame:SetScript("OnDragStart", addonFrame.StartMoving)
	addonFrame:SetScript("OnDragStop", addonFrame.StopMovingOrSizing)

	theText = addonFrame:CreateFontString(nil, "OVERLAY")

	theText:SetFont("Fonts\\ARIALN.TTF", miStatsFontSize)
	theText:SetShadowOffset(1.3,-1.3)
	theText:SetShadowColor(0,0,0)
	setmiStatsTextColor()
	theText:SetJustifyH("LEFT")


	theText:SetPoint("LEFT", addonFrame)
	theText:SetPoint("TOP", addonFrame)


	addonFrame:SetScript("OnUpdate", updateGameLoop)

	-- register for events
	addonFrame:SetScript("OnEvent", function(self, event, ...)
		events[event](self, ...); -- call one of the functions above
	end);
	for key, value in pairs(events) do
		addonFrame:RegisterEvent(key); -- Register all events for which handlers have been defined
	end

end


function miStatsMain()
	initialValues()
	createFrame()
	calculateToLevels()
	inited = true

end

local function shadowUpdate()
	if miStatsShadow then
		theText:SetShadowOffset(1.3,-1.3)
	else
		theText:SetShadowOffset(0,0)
	end

end

local function resetDefaults()
	for k,value in pairs(miStatsToggles) do
		miStatsToggles[k] = true
	end
	miStatsOneLine = false
	miStatsShadow = true
end

local function printHelp()
	local strs = {}
	table.insert(strs, "The following commands are valid:\n")
	table.insert(strs, "/mistats fps")
	table.insert(strs, "/mistats lag")
	table.insert(strs, "/mistats ktl")
	table.insert(strs, "/mistats qtl")
	table.insert(strs, "/mistats azdtl")
	table.insert(strs, "/mistats azwqtl")
	table.insert(strs, "/mistats azitl")
	table.insert(strs, "--the above toggle the display of those values\n")
	table.insert(strs, "/mistats avg/average")
	table.insert(strs, "--toggles displaying the average per measured value\n")
	table.insert(strs, "/mistats shadow")
	table.insert(strs, "--toggles the text shadow\n")
	table.insert(strs, "/mistats oneline")
	table.insert(strs, "--toggles displaying text in paragraph format or just one line\n")
	table.insert(strs, "/mistats reset")
	table.insert(strs, "--resets all settings to the default\n")
	table.insert(strs, "/mistats color *colorname/classname*")
	table.insert(strs, "--sets text to the color specified (/mistats color green)\n")
	table.insert(strs, "/mistats update")
	table.insert(strs, "--forces a stat refresh\n")
	table.insert(strs, "/mistats reset cache")
	table.insert(strs, "/mistats reset allcache")
	table.insert(strs, "--resets cache that allows kill tracking/resets kills xp cache and quests xp cache\n")
	table.insert(strs, "/mistats debug cache quests/kills/dungeons/ap/*nothing*")
	table.insert(strs, "--prints to chat window the cached list of xp amounts for any argument (or all if nothing is specified)\n")
	table.insert(strs, "/mistats debug")
	table.insert(strs, "--toggles debug mode\n")

	for i,v in ipairs(strs) do
		print(v)
	end

end

local function printQuestCache()
	print("----QuestCache-----")
	for i,v in ipairs(miStatsXPCache[kCacheQuestsStr]) do
		print(i .. ": ", v)
	end
	local qAvgMean = calculateAverage(miStatsXPCache[kCacheQuestsStr], kMethodMean)
	local qAvgMed = calculateAverage(miStatsXPCache[kCacheQuestsStr], kMethodMedian)
	local remaining = levelMaxXP - xp
	print("--QuestStats--")
	print("Mean: " .. qAvgMean .. " MeanQTL: " .. remaining / qAvgMean)
	print("Median: " .. qAvgMed .. " MedianQTL: " .. remaining / qAvgMed)
end

local function printKillCache()
	print("----KillCache-----")
	for i,v in ipairs(miStatsXPCache[kCacheKillsStr]) do
		print(i .. ": ", v)
	end
	local kAvgMean = calculateAverage(miStatsXPCache[kCacheKillsStr], kMethodMean)
	local kAvgMed = calculateAverage(miStatsXPCache[kCacheKillsStr], kMethodMedian)
	local remaining = levelMaxXP - xp
	print("--KillStats--")
	print("Mean: " .. kAvgMean .. " MeanKTL: " .. remaining / kAvgMean)
	print("Median: " .. kAvgMed .. " MedianKTL: " .. remaining / kAvgMed)
end

local function printXPDungeonCache()
	print("----DungeonByDungeonCache-----")
	for mapId,instanceDungeonCache in pairs(dungeonCache) do
		print(mapId .. ": ")
		local total = 0
		for i,xpGain in ipairs(instanceDungeonCache) do
			total = total + xpGain
			print(i .. ": ", xpGain)
		end
		print(mapId .. " total:", total)
	end
	print("----DungeonCache-----")
	for i,v in ipairs(miStatsXPCache[kCacheDungeonsStr]) do
		print(i .. ": ", v)
	end
	local dAvgMean = calculateAverage(miStatsXPCache[kCacheDungeonsStr], kMethodMean)
	local dAvgMed = calculateAverage(miStatsXPCache[kCacheDungeonsStr], kMethodMedian)
	local remaining = levelMaxXP - xp
	print("--DungeonStats--")
	print("Mean: " .. dAvgMean .. " MeanDTL: " .. remaining / dAvgMean)
	print("Median: " .. dAvgMed .. " MedianDTL: " .. remaining / dAvgMed)
end

local function printAPCache()
	print("----APInstanceCache-----")
	for mapId,thisInstanceCache in pairs(azInstanceCache) do
		print(mapId .. ": ")
		local total = 0
		for i,apGain in ipairs(thisInstanceCache) do
			total = total + apGain
			print(i .. ": ", apGain)
		end
		print(mapId .. " total:", total)
	end

	print("----APWQCache-----")
	for i,v in ipairs(miStatsXPCache[kCacheAzWQuestsStr]) do
		print(i .. ": ", v)
	end
	local dAvgMean = calculateAverage(miStatsXPCache[kCacheAzWQuestsStr], kMethodMean)
	local dAvgMed = calculateAverage(miStatsXPCache[kCacheAzWQuestsStr], kMethodMedian)
	local remaining = maxAzeriteXP - currentAzeriteXP
	print("--WQStats--")
	print("Mean: " .. dAvgMean .. " MeanDTL: " .. remaining / dAvgMean)
	print("Median: " .. dAvgMed .. " MedianDTL: " .. remaining / dAvgMed)

	print("----APIslandCache-----")
	for i,v in ipairs(miStatsXPCache[kCacheAzIslandsStr]) do
		print(i .. ": ", v)
	end
	dAvgMean = calculateAverage(miStatsXPCache[kCacheAzIslandsStr], kMethodMean)
	dAvgMed = calculateAverage(miStatsXPCache[kCacheAzIslandsStr], kMethodMedian)
	remaining = maxAzeriteXP - currentAzeriteXP
	print("--APIslandStats--")
	print("Mean: " .. dAvgMean .. " MeanDTL: " .. remaining / dAvgMean)
	print("Median: " .. dAvgMed .. " MedianDTL: " .. remaining / dAvgMed)

	print("----APDungeonCache-----")
	for i,v in ipairs(miStatsXPCache[kCacheAzDungeonsStr]) do
		print(i .. ": ", v)
	end
	dAvgMean = calculateAverage(miStatsXPCache[kCacheAzDungeonsStr], kMethodMean)
	dAvgMed = calculateAverage(miStatsXPCache[kCacheAzDungeonsStr], kMethodMedian)
	remaining = maxAzeriteXP - currentAzeriteXP
	print("--APDungeonStats--")
	print("Mean: " .. dAvgMean .. " MeanDTL: " .. remaining / dAvgMean)
	print("Median: " .. dAvgMed .. " MedianDTL: " .. remaining / dAvgMed)


end

local function argHandler(msg)
	local argsMatch = string.gmatch(msg, "%S+")
	local args = {}

	for i in argsMatch do
		-- print(i)
		table.insert(args, i)
	end
	return args[1], args[2], args[3]
	-- print("command, arg1, arg2", command, arg1, arg2)
end

function slashHandler(msg)
	local lowerMsg = strlower(msg)
	local command, arg1, arg2 = argHandler(lowerMsg)
	-- print("command, arg1, arg2", command, arg1, arg2)

	if command == kToggleKTLStr or command == "ktl" then
		miStatsToggles[kToggleKTLStr] = not miStatsToggles[kToggleKTLStr]
		print("Toggled 'ktl'")
	elseif command == kToggleQTLStr or command == "qtl" then
		miStatsToggles[kToggleQTLStr] = not miStatsToggles[kToggleQTLStr]
		print("Toggled 'qtl'")
	elseif command == kToggleLagStr or command == "home" or command == "world" then
		miStatsToggles[kToggleLagStr] = not miStatsToggles[kToggleLagStr]
		print("Toggled 'lag'")
	elseif command == kToggleDTLStr or command == "dtl" then
		miStatsToggles[kToggleDTLStr] = not miStatsToggles[kToggleDTLStr]
		print("Toggled 'dtl'")
	elseif command == kTogglesFPSStr then
		miStatsToggles[kTogglesFPSStr] = not miStatsToggles[kTogglesFPSStr]
		print("Toggled 'fps'")
	elseif command == kToggleAzDTLStr or command == "azdtl" then
		miStatsToggles[kToggleAzDTLStr] = not miStatsToggles[kToggleAzDTLStr]
		print("Toggled 'azdtl'")
	elseif command == kToggleAzWQTLStr or command == "azwqtl" then
		miStatsToggles[kToggleAzWQTLStr] = not miStatsToggles[kToggleAzWQTLStr]
		print("Toggled 'azwqtl'")
	elseif command == kToggleAzITLStr or command == "azitl" then
		miStatsToggles[kToggleAzITLStr] = not miStatsToggles[kToggleAzITLStr]
		print("Toggled 'azitl'")
	elseif command == strlower(kTogglesShowAvg) or command == "showaverage" or command == "avg" or command == "average" then
		miStatsToggles[kTogglesShowAvg] = not miStatsToggles[kTogglesShowAvg]
		print("Toggled showing averages")
	elseif command == "oneline" then
		miStatsOneLine = not miStatsOneLine
		print("Toggled 'oneLine'")
	elseif command == "shadow" then
		miStatsShadow = not miStatsShadow
		print("Toggled 'shadow'")
		shadowUpdate()
	elseif command == "reset" and arg1 == "cache" then
		guidCache = {}
		print("guidCache reset")
	elseif command == "reset" and arg1 == "allcache" then
		guidCache = {}
		resetXPCache()
		print("guidCache and xp cache reset")
		calculateToLevels()
	elseif command == "reset" and arg1 == "default" then
		resetDefaults()
		print("reset to default settings")
	elseif command == "update" or command == "refresh" then
		calculateToLevels()
		print("Refreshed the stat counters") -- all commands refresh - this is just cake
	elseif command == "debug" and arg1 == "cache" and arg2 == "quests" then
		printQuestCache()
	elseif command == "debug" and arg1 == "cache" and arg2 == "kills" then
		printKillCache()
	elseif command == "debug" and arg1 == "cache" and arg2 == "dungeons" then
		printXPDungeonCache()
	elseif command == "debug" and arg1 == "cache" and arg2 == "ap" then
		printAPCache()
	elseif command == "debug" and arg1 == "cache" then
		printQuestCache()
		printKillCache()
	elseif command == "debug" and arg1 == nil then
		miDebugMode = not miDebugMode
		print("debugMode: ", miDebugMode)
	elseif command == "color" and arg1 ~= nil then
		setmiStatsTextColor(arg1)
	else
		printHelp()
	end
	updateStats()
end
